package cjt.lab_4.model;

import javax.persistence.*;

@Entity
@Table(name = "reviews")
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "gender")
    private String gender;

    @Column(name = "age")
    private int age;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @Column(name = "review")
    private String review;

    // Геттер для id
    public int getId() {
        return id;
    }

    // Сеттер для id
    public void setId(int id) {
        this.id = id;
    }

    // Геттер для lastName
    public String getLastName() {
        return lastName;
    }

    // Сеттер для lastName
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    // Геттер для firstName
    public String getFirstName() {
        return firstName;
    }

    // Сеттер для firstName
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    // Геттер для gender
    public String getGender() {
        return gender;
    }

    // Сеттер для gender
    public void setGender(String gender) {
        this.gender = gender;
    }

    // Геттер для age
    public int getAge() {
        return age;
    }

    // Сеттер для age
    public void setAge(int age) {
        this.age = age;
    }

    // Геттер для phone
    public String getPhone() {
        return phone;
    }

    // Сеттер для phone
    public void setPhone(String phone) {
        this.phone = phone;
    }

    // Геттер для email
    public String getEmail() {
        return email;
    }

    // Сеттер для email
    public void setEmail(String email) {
        this.email = email;
    }

    // Геттер для review
    public String getReview() {
        return review;
    }

    // Сеттер для review
    public void setReview(String review) {
        this.review = review;
    }
}


