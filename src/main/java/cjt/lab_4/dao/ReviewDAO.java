package cjt.lab_4.dao;

import cjt.lab_4.model.Review;
import java.util.List;

public interface ReviewDAO {
    void addReview(Review review);
    Review getReview(int id);
    List<Review> getAllReviews();
    void updateReview(Review review);
    void deleteReview(int id);
}

