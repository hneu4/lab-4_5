package cjt.lab_4.dao;

import cjt.lab_4.model.Review;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class ReviewDAOImpl implements ReviewDAO {
    private SessionFactory sessionFactory;

    public ReviewDAOImpl() {
        this.sessionFactory = new Configuration().configure().buildSessionFactory();
    }

    @Override
    public void addReview(Review review) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(review);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public Review getReview(int id) {
        Session session = sessionFactory.openSession();
        Review review = session.get(Review.class, id);
        session.close();
        return review;
    }

    @Override
    public List<Review> getAllReviews() {
        Session session = sessionFactory.openSession();
        List<Review> reviews = session.createQuery("from Review", Review.class).list();
        session.close();
        return reviews;
    }

    @Override
    public void updateReview(Review review) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.update(review);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void deleteReview(int id) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Review review = session.get(Review.class, id);
        if (review != null) {
            session.delete(review);
        }
        session.getTransaction().commit();
        session.close();
    }
}
