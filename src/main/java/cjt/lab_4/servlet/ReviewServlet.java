package cjt.lab_4.servlet;

import cjt.lab_4.dao.ReviewDAO;
import cjt.lab_4.dao.ReviewDAOImpl;
import cjt.lab_4.model.Review;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ReviewServlet", urlPatterns = {"/review"})
public class ReviewServlet extends HttpServlet {
    private ReviewDAO reviewDAO;

    @Override
    public void init() {
        reviewDAO = new ReviewDAOImpl();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            action = "list";
        }
        switch (action) {
            case "new":
                showNewForm(request, response);
                break;
            case "edit":
                showEditForm(request, response);
                break;
            case "delete":
                deleteReview(request, response);
                break;
            default:
                listReviews(request, response);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        switch (action) {
            case "insert":
                insertReview(request, response);
                break;
            case "update":
                updateReview(request, response);
                break;
            default:
                doGet(request, response);
                break;
        }
    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("review", null);
        request.getRequestDispatcher("review-create-form.jsp").forward(request, response);
    }

    private void insertReview(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String lastName = request.getParameter("lastName");
        String firstName = request.getParameter("firstName");
        String gender = request.getParameter("gender");
        int age = Integer.parseInt(request.getParameter("age"));
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        String review = request.getParameter("review");

        Review newReview = new Review();
        newReview.setLastName(lastName);
        newReview.setFirstName(firstName);
        newReview.setGender(gender);
        newReview.setAge(age);
        newReview.setPhone(phone);
        newReview.setEmail(email);
        newReview.setReview(review);

        reviewDAO.addReview(newReview);
        response.sendRedirect("review?action=list");
    }

    private void deleteReview(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        reviewDAO.deleteReview(id);
        response.sendRedirect("review?action=list");
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Review existingReview = reviewDAO.getReview(id);
        request.setAttribute("review", existingReview);
        request.getRequestDispatcher("review-edit-form.jsp").forward(request, response);
    }

    private void updateReview(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Review review = reviewDAO.getReview(id);
        review.setLastName(request.getParameter("lastName"));
        review.setFirstName(request.getParameter("firstName"));
        review.setGender(request.getParameter("gender"));
        review.setAge(Integer.parseInt(request.getParameter("age")));
        review.setPhone(request.getParameter("phone"));
        review.setEmail(request.getParameter("email"));
        review.setReview(request.getParameter("review"));

        reviewDAO.updateReview(review);
        response.sendRedirect("review?action=list");
    }

    private void listReviews(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Review> listReviews = reviewDAO.getAllReviews();
        request.setAttribute("reviews", listReviews);

        request.getRequestDispatcher("review-list.jsp").forward(request, response);
    }
}
