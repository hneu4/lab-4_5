<!DOCTYPE html>
<html>
<head>
    <title>Add/Edit reviews</title>
</head>
<body>
<h1>Add Review</h1>
<form action="review?action=insert" method="post">
    <label>Surname:</label>
    <label>
        <input type="text" name="lastName">
    </label>
    <br>
    <label>Name:</label>
    <label>
        <input type="text" name="firstName">
    </label>
    <br>
    <label>Gender:</label>
    <label>
        <input type="text" name="gender">
    </label>
    <br>
    <label>Age:</label>
    <label>
        <input type="number" name="age">
    </label>
    <br>
    <label>Phone number:</label>
    <label>
        <input type="text" name="phone">
    </label>
    <br>
    <label>Email:</label>
    <label>
        <input type="email" name="email">
    </label>
    <br>
    <label>Review:</label>
    <label>
        <textarea name="review"></textarea>
    </label>
    <br>
    <button type="submit">Send</button>
    <button type="reset">Clear</button>
</form>
<a href="review?action=list">Back to review list</a>
</body>
</html>
