<jsp:useBean id="review" scope="request" type="cjt.lab_4.model.Review"/>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
    <title>Add/Edit reviews</title>
</head>
<body>
<h1>${review.id == null ? 'Add' : 'Edit'} Review</h1>
<form action="review?action=${review.id == null ? 'insert' : 'update'}" method="post">
    <input type="hidden" name="id" value="${review.id}">
    <label>Surname:</label>
    <label>
        <input type="text" name="lastName" value="${review.lastName}">
    </label>
    <br>
    <label>Name:</label>
    <label>
        <input type="text" name="firstName" value="${review.firstName}">
    </label>
    <br>
    <label>Gender:</label>
    <label>
        <input type="text" name="gender" value="${review.gender}">
    </label>
    <br>
    <label>Age:</label>
    <label>
        <input type="number" name="age" value="${review.age}">
    </label>
    <br>
    <label>Phone number:</label>
    <label>
        <input type="text" name="phone" value="${review.phone}">
    </label>
    <br>
    <label>Email:</label>
    <label>
        <input type="email" name="email" value="${review.email}">
    </label>
    <br>
    <label>Review:</label>
    <label>
        <textarea name="review">${review.review}</textarea>
    </label>
    <br>
    <button type="submit">Send</button>
    <button type="reset">Clear</button>
</form>
<a href="review?action=list">Back to review list</a>
</body>
</html>
