<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java"  pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <title>Review list</title>
</head>
<body>
<h1>Review list</h1>
<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>Surname</th>
        <th>Name</th>
        <th>Gender</th>
        <th>Age</th>
        <th>Phone Number</th>
        <th>Email</th>
        <th>Review</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${requestScope.reviews}" var="review" >
        <tr>
            <td><c:out value="${review.id}" /></td>
            <td><c:out value="${review.lastName}" /></td>
            <td>${review.firstName}</td>
            <td>${review.gender}</td>
            <td>${review.age}</td>
            <td>${review.phone}</td>
            <td>${review.email}</td>
            <td>${review.review}</td>
            <td>
                <a href="review?action=edit&id=${review.id}">Edit</a>
                <a href="review?action=delete&id=${review.id}">Delete</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<a href="index.jsp">Home Page</a>
</body>
</html>
